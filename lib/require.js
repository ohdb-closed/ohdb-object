/**
 * lib/require.js -- ohdb-object
 *
 * Created by Karim Alibhai.
 * Copyright (C) OHDB 2015.
 **/

(function () {
    "use strict";

    var OHDB = global.OHDB || {},
        modules = {},
        modulify = function (name, fn) {
            var url = 'http://console.ohdb.ca/module?name=' + name + '&_ts=' + (+new Date()),
                xhr = new XMLHttpRequest();

            // configure xhr
            xhr.open('get', url, true);
            xhr.onreadystatechange = function () {
                /*jslint evil:true*/
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        fn(eval(xhr.responseText));
                    } else {
                        OHDB.error('Faild to fetch module: ' + name + '.');
                    }
                }
                /*jslint evil:false*/
            };
            xhr.send(null);
        };

    OHDB.require = function (reqs, callback) {
        var deps = [], i = -1, next = function () {
            i += 1;

            if (i < reqs.length) {
                reqs[i] = String(reqs[i]);

                if (modules.hasOwnProperty(reqs[i])) {
                    deps.push(modules[reqs[i]]);
                    next();
                } else {
                    modulify(reqs[i], function (mod) {
                        modules[reqs[i]] = mod;
                        deps.push(mod);
                        next();
                    });
                }
            } else {
                callback.apply(global, deps);
            }
        };

        // prefer arrays
        if (!reqs instanceof Array) {
            reqs = [reqs];
        }

        next();
    };

    // global export
    OHDB = global.OHDB;
}());
