/**
 * lib/events.js -- ohdb-object
 *
 * Created by Karim Alibhai.
 * Copyright (C) OHDB 2015.
 **/

(function () {
    "use strict";

    var OHDB = global.OHDB || {},
        EventEmitter = require('events').EventEmitter,
        emitter = new EventEmitter();

    // event piping
    OHDB.on = emitter.on.bind(emitter);
    OHDB.once = emitter.once.bind(emitter);
    OHDB.off = emitter.off.bind(emitter);
    OHDB.emit = emitter.emit.bind(emitter);

    // error-handling
    OHDB.error = function (msg) {
        var err = new Error(msg);

        if (!OHDB.emit('error', err)) {
            throw err;
        }
    };

    // global export
    global.OHDB = OHDB;
}());
