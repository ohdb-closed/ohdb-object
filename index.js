/**
 * index.js -- ohdb-object
 *
 * Created by Karim Alibhai.
 * Copyright (C) OHDB 2014.
 **/
/*jslint node:true*/

(function () {
    "use strict";

    // load object properties
    require('./lib/events.js');
    require('./lib/require.js');

}());
