/**
 * gulpfile.js -- ohdb-object
 *
 * Created by Karim Alibhai.
 * Copyright (C) OHDB 2014.
 **/
/*jslint node:true*/

(function () {
    "use strict";

    var gulp = require('gulp'),
        jslint = require('gulp-jslint'),
        uglify = require('gulp-uglify'),
        browserify = require('browserify'),
        source = require('vinyl-source-stream'),
        buffer = require('vinyl-buffer');

    gulp.task('jslint', function () {
        return gulp.src(['gulpfile.js', 'index.js', 'lib/*.js']).pipe(jslint());
    });

    gulp.task('browserify', function () {
        return browserify('./index.js').bundle()
                   .pipe(source('./index.min.js'))
                   .pipe(buffer())
                   .pipe(uglify())
                   .pipe(gulp.dest('./'));
    });

    gulp.task('default', ['jslint', 'browserify']);
}());
