# ohdb-object

The browser-side client for the ohdb console.

# contributors

- Alina Jahani <alinajahani@gmail.com>
- Mahimul Hoque <mahimulhoque@gmail.com>
- Karim Alibhai <k4rim.sa@gmail.com>